# Bitmap Converter for Nokia 3310 LCD Screen
This is a simple program to take any JPG file, shrink it to the appropriate size (84x60), and then convert it into a monochrome BMP file appropriate for display on the Nokia 3310.
###Prerequisites
-Python3
-VirtualEnvironment