from PIL import Image

print("***********************************\n*                                 *\n* NOKIA 3310 LCD IMAGE CONVERTER  *\n*                                 *\n*       @GITMEDS                  *\n***********************************\n")

location = input("Path to File > ")
im = Image.open(location)

print("\n***********************************\nCurrent Image Details\n")
print("WIDTH AND HEIGHT IN PIXELS")
print(im.size)
print("FILE FORMAT")
print(im.format)
print("COLOR MODE")
print(im.mode)
print("***********************************\n")
input("\nPress any key to continue...")
print("\nTARGET WIDTH")

width_target = int(input("End File Width (px): "))
height_target = int(input("End File Height (px): "))

out = im.resize((width_target,height_target))

file_name = input("File Name? : ")
file_format = ".bmp"
file_out = file_name + file_format
out.convert('1').save(file_out)

#Convert Bitmap to Array
#For Nokia 3310 - Limited to 504-Byte Array of Char
#Vertical Byte Orientation
#Size Endianness = Little
input("Byte Array Test")
with open ("file_out","rb") as imageFile:
    f = imageFile.read()
    b = bytearray(f)

print b[0]